# HOW TO CREATE A POST

You can find all the posts in `content` folder. Once there, you'll have to select the folder where you want to post it. (For example: Release Notes):

```
$HUGO_PROJECT/content/Release-Notes
```

Then, you can create a `markdown file` using this command:

```
hugo new $HUGO_PROJECT/content/Release-Notes/[post-name].md
```

In the file content, you'll find the metadata of the post:

```
---
title: "Release Notes 1.0.7"
date: 2020-05-06T09:18:24+02:00
draft: true
---
```
In order to display the post, draft must be `false`.

Moreover, you can add others parameters as:
- `linktitle`: custom relative URL
- `weight`: it is used to sort the post. A higher number will be lower on the list. This order depends to date too.
- `categories`
- `tags`

Then write the post and save.

At the end, commit the new files and push them into the project.
